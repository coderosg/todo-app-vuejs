import {makeLocalStorage} from "./index";


describe('unit tests for makeLocalStorage function', () => {
    let mockTodoList = []
    beforeEach(() => {
        mockTodoList = [
            { id: 1, name: 'buy bread' },
            { id: 2, name: 'do some tests'}
        ]
    })

    test('should be null init value', () => {
        const storage = makeLocalStorage('todo-list');
        expect(storage.getAll()).toBeNull()
    })
    test('should set new list', () => {
        const storage = makeLocalStorage('todo-list');
        storage.set(mockTodoList)
        const todoList = storage.getAll();
        const newTodoList = todoList.slice().push({ id: 3, name: 'new todo' })
        storage.set(newTodoList)
        expect(storage.getAll().length).toBe(newTodoList.length)
    })
    test('should set one item', () => {
        const storage = makeLocalStorage('todo-list'),
              newTodo = { id: 85, name: 'some todo' }
        storage.set(mockTodoList)
        const todoList = storage.getAll();
        storage.addOne(newTodo)
        expect(storage.getAll().length).toBe(todoList.length + 1)
        expect(storage.getAll().pop()).toStrictEqual(newTodo)
    })
    test('should return item by id', () => {
        const storage = makeLocalStorage('todo-list');
        storage.set(mockTodoList)
        const todo = storage.getById(1);
        expect(todo).toBeDefined();
        expect(todo.id).toBe(1)
    })
    test('should return error with wrong id on getById', () => {
        const storage = makeLocalStorage('todo-list');
        storage.set(mockTodoList)
        const todo = storage.getById(99);
        expect(todo).toBeUndefined();
    })
    test('should clear storage', () => {
        const storage = makeLocalStorage('todo-list');
        storage.set(mockTodoList);
        storage.clear();
        expect(storage.getAll().length).toBe(0)
    })
    test('should remove by id', () => {
        const storage = makeLocalStorage('todo-list')
        storage.set(mockTodoList);
        expect(storage.getById(1)).toBeDefined();
        storage.removeById(1)
        expect(storage.getById(1)).toBeUndefined()
    })
})


