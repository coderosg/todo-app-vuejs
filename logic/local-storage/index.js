const makeLocalStorage = (name) => {
    const storage = window.localStorage,
        initValue = storage.getItem(name) || "[]"
    storage.setItem(name, initValue)
    return {
        set(value) {
            storage.setItem(name, JSON.stringify(value))
        },
        addOne(todo) {
            const todoList = this.getAll().slice()
            todoList.push(todo)
            storage.setItem(name, JSON.stringify(todoList))
        },
        getAll() {
            const items = storage.getItem(name);
            return JSON.parse(items)
        },
        getById(id) {
            const todoList = this.getAll();
            return todoList.find(todo => todo.id === id);
        },
        clear() {
            storage.setItem(name, JSON.stringify([]))
        },
        removeById(id) {
            let todoList = this.getAll();
            const todo = todoList.find(todo => todo.id === id);
            if (todo) {
                todoList.splice(todoList.indexOf(todo), 1)
                this.set(todoList)
            } else {
                console.error(
                    `Error in removeById, todo with  id: ${id} - not found`
                )
            }
        }
    }
}

export {makeLocalStorage}
