import {createLocalVue} from "vue-test-utils";
import state from './modules/todo/state'
import mutations from './modules/todo/mutations'
import {cloneDeep} from 'lodash'
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('test vuex store', () => {

    let store
    beforeEach(() => {
        store = new Vuex.Store({
            state,
            mutations
        })
    })

    it('should add new todo to state', () => {
        const newTodo = { id: 1, name: 'do some tests' }
        store.commit('SET_TODO', newTodo)
        // expect(store.state.todoList).toStrictEqual([newTodo])
    })
})
