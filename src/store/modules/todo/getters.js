export default {
    doneTodoList(state, getters) {
        return state.todoList.filter(todo => todo.done)
    }
}
