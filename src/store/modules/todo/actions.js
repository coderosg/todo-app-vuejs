export default {
    addTodo({state, commit}, todo) {
        const todoList = state.list.push(todo)
        commit('SET_TODO_LIST', todoList)
    },
    removeTodo({state, commit}, todo) {
        let todoList = state.list.slice();
        todoList.splice(state.list.indexOf(todo), 1)
        commit('SET_TODO_LIST', todoList)
    },
    clearTodoList({commit}) {
        commit('SET_TODO_LIST', [])
    },
}
