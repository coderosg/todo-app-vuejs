import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import {makeLocalStorage} from "../logic/local-storage";
import App from './App.vue'
import store from './store'

Vue.use(ElementUI)

const eventBus = new Vue()

Vue.config.productionTip = false
Vue.prototype.$todoLocalStorage = makeLocalStorage('todo-list')
Vue.prototype.$eventBus = eventBus

new Vue({
  store,
  render: h => h(App),
}).$mount('#app')
