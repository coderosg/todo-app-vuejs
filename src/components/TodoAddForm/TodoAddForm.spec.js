import {shallowMount, createLocalVue} from "@vue/test-utils";
import TodoAddForm from "@/components/TodoAddForm/TodoAddForm";
import state from '@/store/modules/todo/state'
import mutations from '@/store/modules/todo/mutations'
import Vuex from 'vuex'
import {stubsElementUI} from "../../../tests/utils";

const localVue = createLocalVue()
localVue.use(Vuex)

describe('unit tests for TodoAddForm component', () => {
    let store;

    beforeEach(() => {
        store = new Vuex.Store({
            state,
            mutations
        })
    })
    it('should add new todo to store', () => {
        const wrapper = shallowMount(TodoAddForm, {stubs: stubsElementUI, store, localVue});
        wrapper.setData({todoName: 'buy bread',})
        wrapper.vm.handleSubmit()
        const lastTodo = store.state.todoList.pop()
        expect(lastTodo.name).toBe('buy bread')
    })
    it('should clear form field name on submit', () => {
        const wrapper = shallowMount(TodoAddForm, {stubs: stubsElementUI, store, localVue});
        wrapper.setData({todoName: 'buy bread',})
        wrapper.vm.handleSubmit();
        expect(wrapper.vm.todoName).toBe('')
    })
})
